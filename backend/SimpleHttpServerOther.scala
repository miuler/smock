import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.{IncomingConnection, ServerBinding}
import akka.http.scaladsl.model.HttpResponse
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}


import scala.concurrent.Future



/**
  * Created by miuler on 5/3/16.
  */
object Main {
  """
    [
      {"head":{"path":"/"}, "body":{}},
      {"head":{}, "body":{}}
    ]
  """

  val validate = """
{
 "utilityId":"2",
 "utilityName":"ComEd",
 "accountNumber":"00140060720170297",
 "customerName":"Beth Anne Foundation",
 "serviceAddressline1":"511 W Eugenia St",
 "serviceAddressline2":"",
  "serviceCityName":"Lombard",
  "servicePostalCode":"60148",
 "serviceState":"OH",
 "billingAddressline1":"525 Roosevelt Rd",
 "billingAddressline2":"",
  "billingCityName":"Glen Ellyn",
  "billingPostalCode":"60137",
 "billingState":"OH",
 "signedDate":"2015-12-29",
 "channel":"D2D",
 "sourceId":"2569",
 "responseCode":"RESENRVAL_OK"
}
                 """

  var mapUrls = Map("/" -> Option("root"), "/holamundo" -> Option("hola mundo"))

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val serverSourceDefualt: Source[IncomingConnection, Future[ServerBinding]] = Http().bind("localhost", 8080)
  val serverSource1: Source[IncomingConnection, Future[ServerBinding]] = Http().bind("localhost", 8388)

  val merge = serverSourceDefualt.merge(serverSource1)
  //

  //  serverSourceDefualt.to(Sink.foreach{ connection =>
  merge.to(Sink.foreach{ connection =>
    println("====================================================================")
    println(s"Accepted new connection from ${connection.remoteAddress} -> ${connection.localAddress}")
    connection.handleWithSyncHandler({ request =>
      println(request.uri.path)
      //      ws.provisioning.uri.selfservice.validate=:8388/NS/Provisioning/Account/Enrollment/Resend/Validation/
      //      ws.provisioning.uri.selfservice.resubmit=:8388/NS/Provisioning/Account/Enrollment/Resend/Rejected

      HttpResponse(200, entity = validate);
      //      val path = request.uri.path.toString()
      //      val uri: Option[String] = mapUrls.getOrElse(path, None)
      //
      //      uri.map(value => {
      //        println(s"call from ${connection.remoteAddress}: ...........................")
      //        println(uri)
      //        println(value)
      //        HttpResponse(200, entity = value)
      //      }).getOrElse(HttpResponse(500, entity = "ERROR"))

    })
  }).run()

}


