name := "miuler-simple-http-akka"

scalaVersion := "2.13.1"


libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.6.1"
libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.11"


