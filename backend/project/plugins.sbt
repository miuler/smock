addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0")
addSbtPlugin("com.github.sbt" % "sbt-jacoco" % "3.2.0")
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.0")
addSbtPlugin("au.com.onegeek" %% "sbt-dotenv" % "2.1.146")

