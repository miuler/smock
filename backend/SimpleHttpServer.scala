import akka.stream.ActorMaterializer
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.ContentType
import akka.http.scaladsl.model._
import akka.event.Logging
import akka.event.LoggingAdapter


/**
 * Created by miuler on 9/3/15.
 */
object SimpleHttpServer extends App {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  val log: LoggingAdapter = Logging.getLogger(system, this)

  val requestHandler: HttpRequest => HttpResponse = { case request =>

    log.info(s"uri: ${request.uri}")
    log.info(s"headers: ${request.headers}")

    request match {

      case HttpRequest(GET, Uri.Path("/timeout/10"), _, _, _) => {
        Thread.sleep(10 * 1000)
        HttpResponse(200, entity = "timeout: " + 10)
      }

      case HttpRequest(GET, Uri.Path("/timeout/5"), _, _, _) => {
        Thread.sleep(5 * 1000)
        HttpResponse(200, entity = "timeout: " + 5)
      }
      
      case HttpRequest(GET, Uri.Path("/timeout/1"), _, _, _) => {
        Thread.sleep(1 * 1000)
        HttpResponse(200, entity = "timeout: " + 1)
      }
      
      case HttpRequest(GET, Uri.Path("/timeout/0.5"), _, _, _) => {
        Thread.sleep(500)
        HttpResponse(200, entity = "timeout: " + 0.5)
      }
      
      case HttpRequest(GET, Uri.Path("/timeout/0.1"), _, _, _) => {
        Thread.sleep(100)
        HttpResponse(200, entity = "timeout: " + 0.1)
      }

      case HttpRequest(GET, Uri.Path("/services2/usages"), _, _, _) =>
        HttpResponse(500, entity = "Unknown error!")
//        sys.error("BOOM!")
      case HttpRequest(POST, Uri.Path("/services2/usages"), _, _, _) =>
        HttpResponse(500, entity = "Unknown error!")
//        sys.error("BOOM!")

      case HttpRequest(GET, Uri.Path("/"), _, _, _) =>
        HttpResponse(entity = HttpEntity(MediaTypes.`text/html`.asInstanceOf[ContentType.NonBinary],
          "<html><body>Hello world!</body></html>"))

      case HttpRequest(GET, Uri.Path("/ping"), _, _, _) =>
        HttpResponse(entity = "PONG!")

      case HttpRequest(POST, Uri.Path("/transactor"), _, _, _) =>
        HttpResponse(409, entity = HttpEntity(ContentTypes.`application/json`,
        """
        {
          "errorCode": "E409_001",
          "systemMessage": "systemMessage: prueba jo",
          "useressage": "useressage: prueba jo",
        }
        """))

      case HttpRequest(GET, Uri.Path("/crash"), _, _, _) =>
        sys.error("BOOM!")

//      case HttpRequest(GET, Uri.Path("/file"), _, _, _) => {
//        val response = HttpResponse(200)
//        response.entity.dataBytes.concat()
//      }

      case _: HttpRequest =>
        HttpResponse(500, entity = HttpEntity(ContentTypes.`application/json`,
          """
        {
          "errorCode": "E500_001",
          "systemMessage": "systemMessage: prueba jo",
          "useressage": "useressage: prueba jo",
        }
        """))
    }
  }

  Http().bindAndHandleSync(requestHandler, "localhost", 8980)

}

