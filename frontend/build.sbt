enablePlugins(ScalaJSPlugin)


name := "Scala.js Tutorial"
scalaVersion := "2.13.1" // or any other Scala version >= 2.10.2
//scalaVersion := "2.12.10" // or any other Scala version >= 2.10.2

// This is an application with a main method

scalaJSUseMainModuleInitializer := true

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.7"
libraryDependencies += "in.nvilla" %%% "monadic-html" % "0.4.0"

